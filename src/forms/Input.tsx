import React from "react";
import { useFormikContext } from "formik";
import { Typography } from "@material-ui/core";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {};

const Input: React.FC<Props> = ({ name, ...props }) => {
  const formik = useFormikContext<any>();
  const fieldProps = formik.getFieldProps(name!);
  const fieldMeta = formik.getFieldMeta(name!);

  return (
    <>
      <input {...props} {...fieldProps} />
      {Boolean(fieldMeta.error) && (
        <Typography color="error">{fieldMeta.error}</Typography>
      )}
    </>
  );
};

export default Input;

import React from "react";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@material-ui/core";

const id = "button-form-id";

const ButtonForm: React.FC = () => {
  return (
    <Dialog open>
      <DialogTitle>React Form</DialogTitle>
      <DialogContent>
        <form id={id} onSubmit={console.log}>
          <div>
            <label htmlFor="email">
              <Typography>Email address</Typography>
            </label>
            <input name="email" type="email" placeholder="Enter email" />
          </div>
          <div>
            <label htmlFor="password">
              <Typography>Password</Typography>
            </label>
            <input name="password" type="password" placeholder="Password" />
          </div>
          <Box py={1} />
        </form>
      </DialogContent>
      <DialogActions>
        <Button type="reset" variant="outlined" color="secondary" form={id}>
          Reset
        </Button>
        <Button type="submit" variant="contained" color="primary" form={id}>
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ButtonForm;

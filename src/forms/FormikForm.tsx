import { Formik } from "formik";
import React from "react";
import * as yup from "yup";
import { Box, Button, makeStyles, Theme, Typography } from "@material-ui/core";
import Input from "./Input";

interface Props {}

interface Values {
  email: string;
  password: string;
}

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .required("Email is required and cannot be empty")
    .email("Email should be a valid email address"),
  password: yup
    .string()
    .required("Required")
    .min(3, "Password should be 3 characters minimum"),
});

const useStyles = makeStyles(({ spacing }: Theme) => ({
  container: {
    width: spacing(50),
    height: spacing(40),
    paddingRight: spacing(2),
    paddingLeft: spacing(2),
    margin: "auto auto",
    "& label": {
      display: "inline-block",
      marginBottom: spacing(0.5),
    },
    "& .form-control": {
      display: "block",
      width: "100%",
      height: spacing(3),
      padding: spacing(1, 1),
      fontSize: spacing(2),
      fontWeight: "400",
      lineHeight: "1.5",
      color: "#495057",
      backgroundColor: "#fff",
      backgroundClip: "padding-box",
      border: "1px solid #ced4da",
      borderRadius: spacing(0.25),
      transition: "border-color .15s ease-in-out,box-shadow .15s ease-in-out",
    },
  },
  formGroup: {
    marginBottom: spacing(1),
  },
}));

const FormikForm: React.FC<Props> = ({ ...props }) => {
  const classes = useStyles();

  const initialValues: Values = {
    email: "",
    password: "",
  };
  const handleSubmit = (values: Values) => {
    console.log(values);
  };

  return (
    <div className={classes.container}>
      <Box py={2} display="flex" alignItems="center">
        <Typography variant="h4">React Formik Form</Typography>
      </Box>
      <Box py={2}>
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          {(formik) => (
            <form onSubmit={formik.handleSubmit}>
              <div className={classes.formGroup}>
                <label htmlFor="email">
                  <Typography>Email address</Typography>
                </label>
                <Input
                  name="email"
                  type="email"
                  className={`form-control`}
                  placeholder="Enter email"
                />
              </div>
              <div className={classes.formGroup}>
                <label htmlFor="password">
                  <Typography>Password</Typography>
                </label>
                <Input
                  name="password"
                  type="password"
                  className={`form-control`}
                  placeholder="Password"
                />
              </div>
              <Box py={1} />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
              >
                Submit
              </Button>
            </form>
          )}
        </Formik>
      </Box>
    </div>
  );
};

export default FormikForm;

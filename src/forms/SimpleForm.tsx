import React from "react";
import { Box, Button, makeStyles, Theme, Typography } from "@material-ui/core";
import { get } from "lodash";
import useForceUpdate from "use-force-update";

interface Props {}

const useStyles = makeStyles(({ spacing }: Theme) => ({
  container: {
    width: spacing(50),
    height: spacing(40),
    paddingRight: spacing(2),
    paddingLeft: spacing(2),
    margin: "auto auto",
    "& label": {
      display: "inline-block",
      marginBottom: spacing(0.5),
    },
    "& .form-control": {
      display: "block",
      width: "100%",
      height: spacing(3),
      padding: spacing(1, 1),
      fontSize: spacing(2),
      fontWeight: "400",
      lineHeight: "1.5",
      color: "#495057",
      backgroundColor: "#fff",
      backgroundClip: "padding-box",
      border: "1px solid #ced4da",
      borderRadius: spacing(0.25),
      transition: "border-color .15s ease-in-out,box-shadow .15s ease-in-out",
    },
  },
  formGroup: {
    marginBottom: spacing(1),
  },
}));

interface Values {
  email: string;
  password: string;
}

interface Errors {
  email: string;
  password: string;
}

interface State {
  formValues: Values;
  formErrors: Errors;
  formValidity: {
    email: boolean;
    password: boolean;
  };
}

type Key = "email" | "password";

const SimpleForm: React.FC<Props> = ({ ...props }) => {
  const classes = useStyles();
  const forceUpdate = useForceUpdate();
  const formStateRef = React.useRef<State>({
    formValues: {
      email: "",
      password: "",
    },
    formErrors: {
      email: "",
      password: "",
    },
    formValidity: {
      email: false,
      password: false,
    },
  });

  const handleValidation = (target: { name: Key; value: string }) => {
    const { name, value } = target;
    const isEmail = name === "email";
    const isPassword = name === "password";
    const emailTest = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    let validity = value.length > 0;
    let fieldValidationError = validity
      ? ""
      : `${name} is required and cannot be empty`;

    if (validity) {
      if (isEmail) {
        validity = emailTest.test(value);
        fieldValidationError = validity
          ? ""
          : `${name} should be a valid email address`;
      }
      if (isPassword) {
        validity = value.length >= 3;
        fieldValidationError = validity
          ? ""
          : `${name} should be 3 characters minimum`;
      }
    }

    formStateRef.current = {
      ...formStateRef.current,
      formErrors: {
        ...formStateRef.current.formErrors,
        [name]: fieldValidationError,
      },
      formValidity: {
        ...formStateRef.current.formValidity,
        [name]: validity,
      },
    };
    forceUpdate();
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const { formValues, formValidity } = formStateRef.current;
    if (Object.values(formValidity).every(Boolean)) {
      // Form is valid
      console.log(formValues);
    } else {
      Object.keys(formValues).forEach((key) => {
        const target = {
          name: key as Key,
          value: get(formValues, key),
        };
        handleValidation(target);
      });
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    const target = { name: name as Key, value };

    const formValues = { ...formStateRef.current.formValues, [name]: value };
    formStateRef.current = {
      ...formStateRef.current,
      formValues,
    };
    forceUpdate();
    handleValidation(target);
  };

  return (
    <div className={classes.container}>
      <Box py={2} display="flex" alignItems="center">
        <Typography variant="h4">React regular form</Typography>
      </Box>
      <Box py={2}>
        <form onSubmit={handleSubmit}>
          <div className={classes.formGroup}>
            <label>
              <Typography>Email address</Typography>
            </label>
            <input
              type="email"
              name="email"
              className={`form-control`}
              placeholder="Enter email"
              onChange={handleChange}
              value={formStateRef.current.formValues.email}
            />
            {Boolean(formStateRef.current.formErrors.email) && (
              <Typography color="error">
                {formStateRef.current.formErrors.email}
              </Typography>
            )}
          </div>
          <div className={classes.formGroup}>
            <label>
              <Typography>Password</Typography>
            </label>
            <input
              type="password"
              name="password"
              className={`form-control`}
              placeholder="Password"
              onChange={handleChange}
              value={formStateRef.current.formValues.password}
            />
            {Boolean(formStateRef.current.formErrors.password) && (
              <Typography color="error">
                {formStateRef.current.formErrors.password}
              </Typography>
            )}
          </div>
          <Box py={1} />
          <Button type="submit" variant="contained" color="primary" fullWidth>
            Submit
          </Button>
        </form>
      </Box>
    </div>
  );
};

export default SimpleForm;

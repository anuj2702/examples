import React from "react";

interface Props {
  blog: {
    id: string;
    title: string;
    description: string;
  };
}

const FunctionBlogComponent: React.FC<Props> = ({ blog }) => {
  return (
    <div>
      <h1>{blog.title}</h1>
      <p>{blog.description}</p>
    </div>
  );
};

interface State {}

class ClassBlogComponent extends React.Component<Props, State> {
  render() {
    const { blog } = this.props;
    return (
      <div>
        <h1>{blog.title}</h1>
        <p>{blog.description}</p>
      </div>
    );
  }
}

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles(
  ({ spacing, breakpoints: { up, down }, palette, typography }: Theme) => ({
    bold: {
      fontWeight: 900,
      fontSize: "16px",
    },
    flex: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
    },
    primary: {
      color: palette.primary.main,
    },
    secondary: {
      color: palette.secondary.main,
    },
    text: {
      color: palette.text.primary,
      ...typography.subtitle2,
    },
  })
);

const Hook: React.FC = () => {
  const classes = useStyles();
  const [count, setCount] = React.useState(1);

  return (
    <div className={clsx(classes.flex, classes.text)}>
      <div className={classes.primary}>The count is {count}</div>
      <div
        className={clsx(classes.secondary, {
          [classes.bold]: count % 3 === 0,
        })}
      >
        If the count is multiple of three, we will become bold else we will
        remain only secondary
      </div>
      <button onClick={() => setCount(count + 1)}>Increase count</button>
    </div>
  );
};

export default Hook;

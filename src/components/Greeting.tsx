import React from "react";

const UserGreeting: React.FC = () => {
  return <h1>Welcome back!</h1>;
};

const GuestGreeting: React.FC = () => {
  return <h1>Please sign up.</h1>;
};

interface Props {
  unreadMessages: string[];
  isLoggedIn: boolean;
}

const Mailbox: React.FC<Props> = ({ unreadMessages }) => {
  return (
    <div>
      <h1>Hello!</h1>
      {unreadMessages.length > 0 && (
        <h2>You have {unreadMessages.length} unread messages.</h2>
      )}
    </div>
  );
};

const Greeting: React.FC<Props> = ({ isLoggedIn }) => {
  if (isLoggedIn) {
    return <UserGreeting />;
  }
  return <GuestGreeting />;
};

export default Greeting;

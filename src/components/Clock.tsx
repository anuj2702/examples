import React, { Component } from "react";

interface Props {}

interface State {
  date: Date;
}

class Clock extends Component<Props, State> {
  timerId: any;
  constructor(props: Props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentDidMount() {
    this.timerId = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  tick = () => {
    this.setState({ date: new Date() });
  };

  render() {
    return (
      <div>
        <h2>The time is {this.state.date.toLocaleTimeString()}</h2>
        <button onClick={this.tick}>Update time</button>
      </div>
    );
  }
}

const FunctionalClock: React.FC<Props> = () => {
  const [state, setState] = React.useState<State>({ date: new Date() });

  React.useEffect(() => {
    const timerId = setInterval(() => tick(), 1000);
    return () => {
      clearInterval(timerId);
    };
  }, []);

  const tick = () => {
    setState({ date: new Date() });
  };

  return (
    <>
      <h2>The time is {state.date.toLocaleTimeString()}</h2>
      <button onClick={tick}>Update time</button>
    </>
  );
};

export default FunctionalClock;

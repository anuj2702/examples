import React from "react";
import "./styles.css";

interface Props {}

const Button: React.FC<Props> = () => {
  const [color, setColor] = React.useState("red");
  const handleClick = () => {
    if (color === "red") {
      setColor("blue");
    } else {
      setColor("red");
    }
  };

  return (
    <button className={`btn ${color}`} onClick={handleClick}>
      Click me
    </button>
  );
};

export default Button;

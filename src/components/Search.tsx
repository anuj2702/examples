import React from "react";
import { useDebouncedCallback } from "use-debounce";
import axios, { CancelToken } from "axios";

const Search: React.FC = () => {
  const [text, setText] = React.useState("");
  const [countries, setCountries] = React.useState([]);

  const { callback } = useDebouncedCallback((searchTerm: string) => {
    if (searchTerm) {
      const source = axios.CancelToken.source();
      if (searchTerm) {
        getCountries(searchTerm, source.token)
          .then(setCountries)
          .catch((e) => {
            if (axios.isCancel(source)) {
              return;
            }
            setCountries([]);
          });
      } else {
        setCountries([]);
      }
    }
  }, 400);

  React.useEffect(() => {
    callback(text);
  }, [text]);

  return (
    <div>
      <input value={text} onChange={(e) => setText(e.target.value)} />
      <p>Counties:</p>
      {countries && countries.length ? (
        <ul>
          {countries.map((country) => (
            <li>{country}</li>
          ))}
        </ul>
      ) : (
        <p>No Countries Found</p>
      )}
    </div>
  );
};

const getCountries = (text: string, token: CancelToken) => {
  return axios
    .get("https://restcountries.eu/rest/v2/name/" + text, {
      cancelToken: token,
    })
    .then(({ data }) => data.map((d: any) => d.name));
};

export default Search;

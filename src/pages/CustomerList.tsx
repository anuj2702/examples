import React from "react";
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";
import { Customer } from "./types";

interface Props {
  customers: Customer[];
}

const CustomerList: React.FC<Props> = ({ customers }) => {
  return (
    <List>
      {customers.map((c) => (
        <ListItem key={c.id} divider>
          <ListItemAvatar>
            <Avatar src={c.profileImage} title={c.name} />
          </ListItemAvatar>
          <ListItemText primary={c.name} secondary={c.email} />
        </ListItem>
      ))}
    </List>
  );
};

export default CustomerList;

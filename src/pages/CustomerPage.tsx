import React from "react";
import faker from "faker";
import { map } from "lodash";
import { Customer } from "./types";
import CustomerList from "./CustomerList";
import {
  Box,
  Grid,
  IconButton,
  makeStyles,
  TextField,
  Theme,
  Typography,
} from "@material-ui/core";
import { AddOutlined, SearchOutlined } from "@material-ui/icons";

interface Props {}

const customers = map(
  Array(30),
  () =>
    ({
      id: faker.random.uuid(),
      profileImage: faker.image.image(),
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      email: faker.internet.email(),
      phone: faker.phone.phoneNumber(),
      country: faker.address.county(),
      active: faker.random.boolean(),
    } as Customer)
);

const useStyles = makeStyles(({ spacing }: Theme) => ({
  button: {
    "& div": {
      borderRadius: "40px",
    },
    "& input": {
      padding: spacing(1.5, 3, 1.5, 1),
    },
  },
}));

const CustomerPage: React.FC<Props> = ({ ...props }) => {
  const classes = useStyles();

  return (
    <Box px={4}>
      <Box px={2} py={4}>
        <Grid container justify="space-between">
          <Grid item xs={3}>
            <Typography variant="h5">Customers</Typography>
          </Grid>
          <Box px={1}>
            <TextField
              variant="outlined"
              className={classes.button}
              placeholder={"Type to search..."}
              InputProps={{ startAdornment: <SearchOutlined color="action" /> }}
            />
            &nbsp; &nbsp;
            <IconButton>
              <AddOutlined />
            </IconButton>
          </Box>
        </Grid>
      </Box>
      <CustomerList customers={customers} />
    </Box>
  );
};

export default CustomerPage;

import React from "react";
import Button from "../components/Button";
import Clock from "../components/Clock";
import Search from "../components/Search";
import CustomerPage from "../pages/CustomerPage";
import SimpleForm from "../forms/SimpleForm";
import Hook from "../components/Hook";
import FormikForm from "../forms/FormikForm";
import ButtonForm from "../forms/ButtonForm";

export default {
  title: "Examples",
  decorators: [(storyFn: () => JSX.Element) => <>{storyFn()}</>],
};

export const Buttons = () => <Button />;
export const TimerClock = () => <Clock />;
export const Debouncing = () => <Search />;
export const Customers = () => <CustomerPage />;
export const Simple = () => <SimpleForm />;
export const BTN = () => <Hook />;
export const Formik = () => <FormikForm />;
export const ButtonF = () => <ButtonForm />;
